<?php

namespace Sports\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;

use Symfony\Component\HttpFoundation\Session;

use Sports\SiteBundle\Entity\Contact;
use Sports\SiteBundle\Form\ContactType;

use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;


/**
 * SearchController
 */
class MainController extends Controller {
	
	/**
	 * Searches a game typed by the user on front page
	 */
	public function indexAction() 
	{
		/* start the session */	
//		$session = new Session();
		//$session->start();
		
		$request = $this->getRequest();
		
		$sport = strtolower(trim($request->get('q')));
		$region = trim($request->get('l'));
		$page = trim($request->get('page'));
		
		if (!$page) {
			$page = 1;
		}
				
		$template = 'SportsSiteBundle:Core:home.html.twig'; 
						
		
		if (!($sport == '' || $region == '')) {
			
			// set the template to searchresult
			$template = 'SportsSiteBundle:Core:searchresult.html.twig';
						
			/* get the region (default Canada) to appened at the end of address */
			$regionSuffix = $this->container->getParameter('region_suffix'); 
			
			$opt = array (
				'address' => urlencode($region . ', ' . $regionSuffix),
				'sensor' => 'false'
			);
			
			
			$toolbox = $this->get('toolbox');
			$location = $toolbox->getLatLon($opt);
			
			/* if location has not been found, then just return error */			
			if ($location['status'] == false) 
			{
				$tr    = $this->get('translator');
				$error = $tr->trans('error.location_not_found');
				
				return $this->render($template, array(
								'q' => ucfirst($sport), 
								'l' => $region,
								'error' => $error));				
			}
						
			/* otherwise, proceed normally, and also query based
			 * on user location i.e. sort based on your location */
			
			$earthRadius = $this->container->getParameter('earth_radius');
			$searchRadius = $this->container->getParameter('search_radius'); 


			/**
			 * Running raw SQL as Doctrine does not support Math functions such as SIN, COS etc.
			 * I Need to move the query to the Model, this is not the best practice
			 * FAT Model, slim controller is the way to go!
			 */
			$pdo = $this->getDoctrine()->getEntityManager()->getConnection();

/* Query printing echoing
echo '
					SELECT  sport.name AS sport_name, sport.path AS sport_path, sport.active AS sport_active, 
						    state.name AS state_name, spot.title, spot.path, spot.address, spot.city, 
						 	spot.description, spot.phone, spot.sport_id, spot.state_id, spot.likes,
						 	spot.lat, spot.lon, spot.active, spot.postalCode, spot.url,
						 	CAST(('. $earthRadius .' * 2 * ASIN(SQRT( POWER(SIN(('. 
								$location['lat'].' - spot.lat) * pi()/180 / 2), 2) + COS(' . $location['lat'] . 
								' * pi()/180) * COS(spot.lat * pi()/180) * POWER(SIN((' . $location['lon'] . 
								' - spot.lon) * pi()/180 / 2), 2) ))) AS decimal(5,2)) AS distance
					
					FROM  spots spot, states state, sports sport
					WHERE spot.sport_id = sport.id 
					AND spot.state_id = state.id 
					AND spot.active = :active 
					AND spot.active = :active 
					AND sport.name = :name 
					HAVING distance < :radius
					ORDER BY distance ASC 
';
*/
			$stmt = $pdo->prepare('
					SELECT  sport.name AS sport_name, sport.path AS sport_path, sport.active AS sport_active, 
						    state.name AS state_name, spot.title, spot.path, spot.address, spot.city, 
						 	spot.description, spot.phone, spot.sport_id, spot.state_id, spot.likes,
						 	spot.lat, spot.lon, spot.active, spot.postalCode, spot.url,
						 	CAST(('. $earthRadius .' * 2 * ASIN(SQRT( POWER(SIN(('. 
								$location['lat'].' - spot.lat) * pi()/180 / 2), 2) + COS(' . $location['lat'] . 
								' * pi()/180) * COS(spot.lat * pi()/180) * POWER(SIN((' . $location['lon'] . 
								' - spot.lon) * pi()/180 / 2), 2) ))) AS decimal(5,2)) AS distance
					
					FROM  spots spot, states state, sports sport
					WHERE spot.sport_id = sport.id 
					AND spot.state_id = state.id 
					AND spot.active = :active 
					AND spot.active = :active 
					AND sport.name = :name 
					HAVING distance < :radius
					ORDER BY distance ASC 
					');
		   $stmt->execute(array(
		   		'active' 	=> 1,
		   		'name' 		=> $sport,
		   		':radius'	=> $searchRadius
		   ));
					

/*			
			$em = $this->getDoctrine()->getEntityManager();
			$qb = $em->createQuery('
					SELECT s, st, sp, (SIN(345)) AS distance  
					FROM SportsSiteBundle:Spot s
					JOIN s.state st
					JOIN s.sport sp
					WHERE s.active = :active
					AND sp.active = :active 
					AND sp.name = :name
					AND distance < :radius
					'
			 	)->setParameters(array(
			 		'radius' => $searchRadius,
					'name' 	 => $sport,
					'active' => 1
				));
*/			

//		   $spots = $qb->getArrayResult(); -- not used anymore
			
//		   $spotList = $qb->getResult();

		   $spotList = $stmt->fetchAll();
		   
		   /* finally, check the number of results returned, if its less than 1
		    * so display error and return */
		   if (count($spotList) < 1) 
		   {
				$tr    = $this->get('translator');
				$error = $tr->trans('error.search_result_insufficient');
				
				return $this->render($template, array(
								'q' => ucfirst($sport), 
								'l' => $region,
								'error' => $error));	
		   }
		   
		   /* otherwise proceed accordingly */
		   $searchLimit = $this->container->getParameter('search_limit');		      
		   $adapter = new ArrayAdapter($spotList);
		   $pagerfanta = new Pagerfanta($adapter);
		   $pagerfanta->setMaxPerPage($searchLimit);
			
			
		  try {
		  	$pagerfanta->setCurrentPage($page);
		  }
		  catch(NotValidCurrentPageException $e) {
		  	throw new NotFoundHttpException();
		  }	
			/*
			$spots = array(
				array (
					'title' 	=> 'Club Toronto',
					'address'	=> '170 John St.,',
					'phone' 	=> '647.770.3426',
					'city'		=> 'Toronto',
					'state'		=> 'ON',
					'likes'		=> '7',
					'comments'  => '17',
					'distance'  => '7.1 km',
					'id'		=> '11810',		
					'path'		=> 'club-toronto'
				),
				
				array (
					'title' 	=> 'Max Cricky',
					'address'	=> '170 John St., Brampton, ON',
					'city'		=> 'Toronto',
					'state'		=> 'ON',
					'phone' 	=> '647.770.3426',
					'likes'		=> '7',
					'comments'  => '17',
					'distance'  => '7.1 km',
					'id'		=> '18901',
					'path' 		=> 'max-cricky'				
				
				)
			);
			*/
			
			/*
			 * So, do a database query and get the results
			 * and then get another template and pass the values there!
			 */			 
			// return new Response('?q = '. $sport . ' &l = ' . $region);
			
	//		$session->set('sport_q', $sport);
	//		$session->set('sport_l', $region);
			
			return $this->render($template, array(
								 	'q' 	 => ucfirst($sport), 
								 	'l' 	 => $region,  
								 	'location' => $location,
									'pagerfanta' => $pagerfanta));					
		}
		
			return $this->render($template, 
					array('q' => ucfirst($sport), 'l' => $region));
		
	}
	
	/**
	 * Action for About Page
	 */
	public function aboutAction() {
		return $this->render('SportsSiteBundle:Core:about.html.twig');		
	}
	
	
	/**
	 * Action for Contact Page
	 */
	 public function contactAction() {

		$contact = new Contact();
		$form = $this->createForm(new ContactType(), $contact);
		
		$request = $this->getRequest();
		
		if ($request->getMethod() == 'POST') {
			$form->bindRequest($request);
			if ($form->isValid()) {
				
				$status = array (
					'class' 	=> 'success',
					'message'	=> 'Thank you for contacting us. Your message has been forwarded.',
				);
			}
			else {
					
				$status = array (
					'class' 	=> 'error' ,
					'message'	=> 'Error Submitting Form. Please Try Again.'
				);
			}
			
			$contactEmail = $this->container->getParameter('contact_email');
			$contactEmailSubject = $this->container->getParameter('email_subject');
			
			$message = \Swift_Message::newInstance()
						->setSubject($contactEmailSubject)
						->setFrom($contact->getEmail())
						->setTo($contactEmail)
						->setBody($contact->getName() . " submitted contact form. " 
								  . $contact->getBody());
			
			$this->get('mailer')->send($message);
			
			
			return $this->render('SportsSiteBundle:Core:contactProcess.html.twig', 
								  array('status' => $status));
			
		}
		
		return $this->render('SportsSiteBundle:Core:contact.html.twig', 
					array ('form' => $form->createView()));
	 	
	 }
	 

	/**
	 * Returns the list of sports available
	 * @param $format Allowed values are: json or html.
	 */
	public function sportsAction($format = 'html') {
		
		/* get the list of sports from database at first */
		$em = $this->getDoctrine()->getEntityManager();		
		$sportsResult = $em->createQuery(
					  'SELECT s.name FROM SportsSiteBundle:Sport s' .
					  ' WHERE s.active = :active '
					  )->setParameter('active', 1)
					   ->getArrayResult();
		
		/* neeed to find a better way to solve it */
		$sportsList = array();
		foreach ($sportsResult as $sport) {
			$sportsList[] = $sport['name'];
		}
		

		if ($format == 'json') 
		{
			$response = new Response(json_encode(
								array('sports' => $sportsList)));
			$response->headers->set('Content-Type', 'application/json');			
			return $response; 
		}
		
		
		/* otherwise return HTML formatted data */
		return $this->render('SportsSiteBundle:Core:sports.html.twig', 
								array('sports' => $sportsList));
	}
}
