<?php

namespace Sports\SiteBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Sports\SiteBundle\Entity\UserSpot;


class SpotController extends Controller {
	
	/**
	 * Manages and Displays a specific spot based on type and path passed
	 * @param $type 	The type/path of the Sport (path)
	 * @param $path		The Path of the Spot
	 */
	public function indexAction($type, $path) {
		
//		$session = new Session();
//		$session->start();
		
		/* Get the data about spot from database and then
		 * pass it to the twig template */
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$qb = $em->createQuery('
					SELECT s, st, sp
					FROM SportsSiteBundle:Spot s
					JOIN s.state st
					JOIN s.sport sp
					WHERE s.path = :path
					AND sp.path = :type	
				'
			 	)->setParameters(array(
					'path' 	=> $path,
					'type'	=> $type 
				));
		
		$spot = $qb->getArrayResult();
		
		if (count($spot) > 0) {
			
			$spotDetails = $spot[0];
			
			/** 
			 * if lat and lon is defined then, enable map (set a value to it) 
			 * as map can be drawn!
			 */ 
			if ($spotDetails['lat'] != 0.0) {
				$spotDetails['map'] = true;
			}
			
			
	//		$sport_q = $session->get('sport_q');
	//		$sport_l = $session->get('sport_l');
			
			return $this->render('SportsSiteBundle:Core:spot.html.twig', array(
							 	'spot' => $spotDetails,
//								'q' => $sport_q,
//								'l' => $sport_l
								));			
		}
		
		
		/* 
		 * spot = false is passed to prevent map from crashing
		 * its a temproary fix for now
		 */		
		$tr = $this->get('translator');
		$error = $tr->trans('error.spot_not_found'); 
		 
		return $this->render('SportsSiteBundle:Core:spot.html.twig',
							array('error' => $error, 'spot' => false));
				
	}

	/**
	 * 
	 */
	public function addAction($id) {
		
		if ($id > 0) {
			//$
		}	
		
		$userSpot = new UserSpot();
	//	$userSpot->
		
	}
	
}
