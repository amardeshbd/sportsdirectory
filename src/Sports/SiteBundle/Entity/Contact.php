<?php

namespace Sports\SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * An Entity corresponding to the Contact Form
 * on the website
 */
class Contact {
	
	/**
	 * name - The name of the person contacting 
	 * email - The email of the person contacting
	 * body - The body/message of the person 
	 */
	protected $name;	
	protected $email;	
	protected $body;
	
	/**
	 * Getter & Setter of all the attributes
	 */
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setEmail($email) {
		$this->email = $email;
	}
	
	public function getEmail() {
		return $this->email;
	}
	
	public function setBody($body) {
		$this->body = $body;
	}
	
	public function getBody() {
		return $this->body;
	}
	
}
