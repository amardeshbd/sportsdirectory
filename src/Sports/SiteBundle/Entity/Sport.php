<?php

namespace Sports\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="sports")
 */ 
class Sport
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */	
	protected $id;
	
	/**
	 * @ORM\Column(type="string", length=200)
	 */
	protected $name;
	
	
	/**
	 * @ORM\Column(type="string", length=200)
	 */
	protected $altName;
	
	
	/**
	 * @ORM\Column(type="string", length=200) 
	 */
	 protected $description;
	 
	 
	 /**
	  * @ORM\Column(type="integer")
	  */
	 protected $active;
	 
	 
	 /**
	  * @ORM\OneToMany(targetEntity="Spot", mappedBy="sports")
	  */
	 protected $spots;
	 
	 
	 /**
	  * @ORM\Column(type="string", length="200")
	  */
	 protected $path;
	 
	 /**
	  * Constructor 
	  */
	 public function __construct()  {
	 	$this->spots = new ArrayCollection();
	 }
	 
	 /**
	  * Getters and Setters
	  */
	 public function setName($n) {
	 	$this->name = $n;
	 }
	 
	 public function getName() {
	 	return $this->name;
	 }
	 
	 
	 public function setActive($status) {
	 	$this->active = $status;
	 }
	 
	 public function getActive() {
	 	return $this->active;
	 }
	 
	 
	 public function getAltName() {
	 	return $this->altName;
	 }
	 	 
	 public function setAltName($aname) {
	 	$this->altName = $aname;
	 } 
	 
	 public function setDescription($desc) {
	 	$this->description = $desc;
	 }
	 
	 public function getDescription() {
	 	return $this->description;
	 }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add spots
     *
     * @param Sports\SiteBundle\Entity\Spot $spots
     */
    public function addSpot(\Sports\SiteBundle\Entity\Spot $spots)
    {
        $this->spots[] = $spots;
    }

    /**
     * Get spots
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getSpots()
    {
        return $this->spots;
    }
	
	/**
	 * Getters and Setters for Path
	 */
	public function setPath($path) {
		$this->path = $path;
	}
	
	
	public function getPath() {
		return $this->path;
	}
}