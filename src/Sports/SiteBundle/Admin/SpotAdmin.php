<?php

namespace Sports\SiteBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use Sports\SiteBundle\Entity\Spot;


class SpotAdmin extends Admin {
	
   /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
			->add('path')
            ->add('addressTitle')
            ->add('address')
			->add('url')
			->add('city')
			->add('postalCode')
			->add('likes')
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Spot Information')
                ->add('title')
				->add('path', null, array('required' => false))
				->add('addressTitle', null, array('required' => false))
				->add('address')
				->add('city')
				->add('postalCode', null, array('required' => false))
				->add('url')
				->add('description')
				->add('phone')
				->add('likes')
				->add('lat')
				->add('lon')
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('address')
			->add('city')
			->add('phone')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('address')
			->add('city')
			->add('phone')
        ;
    }

}
